---
layout: post
title: "La résolution de conflits dans un groupe d'humains"
date: 2018-08-27 modification le 14 02 2020
lang: fr
ref: la-resolution-de-conflits-dans-un-groupe-dhumains
author: Yannick=categories: solucracy
observateur: bruno tison
---

#### Introduction

A partir du moment où un groupe d'humains doit travailler ensemble pour atteindre un objectif, la question des modalités de coopération se pose. Je ne vais pas discuter ici des modèles de gouvernance mais plutôt du cas particulier (mais non moins fréquent) où un conflit survient.

Suite à diverses expériences dans des groupes auxquels j'appartiens, il est apparu qu'un unique conflit, que ce soit entre 2 ou plusieurs personnes, peut rapidement bloquer tout progrès dans ce groupe, voire même détruire sa structure et ruiner tous les efforts accomplis.

N'étant aucunement expert dans le domaine, j'ai lancé un appel à témoignages pour tenter de réunir plus de données sur ce genre de situations et voir si d'autres ont su surmonter ces obstacles, protéger les intérêts du groupe, limiter la perte d'énergie et peut-être même renforcer le groupe.

#### Le questionnaire

Si vous désirez recueillir plus de données, vous pouvez partager ce [lien ](https://goo.gl/forms/LGeLyALgCb5eWCq92)

La version originale contenait la possibilité de laisser son adresse email pour obtenir ensuite un accès aux données mais je l'ai retirée et ai supprimé les adresses après avoir notifié les personnes concernées.

L'idée était vraiment de lister un maximum de situations possibles pour voir s'il était possible d'en extraire des tendances ou, qui sait, une solution miracle :-) .

#### Les réponses

Merci à toutes les personnes qui ont contribué. Ca n'a l'air de rien comme ça, mais ça permettra à beaucoup de gens de mettre les choses en perspective et, qui sait, de surmonter plus facilement ces défis. Si vous désirez accéder aux données, vous pouvez les retrouver sur ce [google sheet](https://docs.google.com/spreadsheets/d/1KQf__qRkt-dM_mvuCLn8iz6slv2seLiZIAzKVih7osM/edit#gid=1490444708)

A l'heure où j'écris ce texte, il y a eu 16 réponses au questionnaire. 7 concernent une association, 3 un groupe informel et 2 une entreprise. Les autres choix (famille, parti politique,collectif et institution publique) ont reçu une réponse chacun.

6 concernent des groupes de moins de 10 personnes, 4 des groupes de 11 à 50, 1 pour un groupe de 50 à 100 et 5 des groupes de + de 100 personnes.

Pour les modes de communication concernés, il ne ressort pas de tendance particulière, le virtuel et les conversations physiques semblent touchés de la même manière. Il faudrait à mon avis creuser un peu plus pour voir si les outils informatiques type Chat et email sont plus souvent sources de malentendus, ou si les conversations physiques et le fait qu'on puisse rapidement dire des choses qu'on regrette sont en cause.

Pour 9 réponses, la source du conflit était le comportement d'une personne, pour 2, un conflit entre 2 personnes et pour d'autres un mélange de conflits, de règles/structure mal comprises/définies et des problèmes d'orientation et stratégie.

Apparemment, une à deux personnes sont le plus souvent à l'origine du conflit, ce qui semble tout à fait normal, et la manière dont va le gérer le groupe définira ensuite si le conflit s'étend ou s'il reste contenu.

Pour le reste des informations, je vais émettre certaines conclusions en dessous mais il est probablement aussi très utile que vous vous fassiez vos propres idées en lisant les réponses.

#### Disclaimer

En rédigeant le paragraphe précédent, je me suis dit qu'il valait peut-être mieux que je précise que je ne suis pas un professionnel de la facilitation ou un expert/coach en gouvernance ou autre. Je m'intéresse beaucoup à la psychologie personnelle ou sociale, et de par mes activités, aie plutôt intérêt à comprendre comment faire fonctionner un groupe d'humains en harmonie, mais je n'en ai pas fait ma profession à proprement parler. Cet article n'a aucune valeur scientifique donc comme pour tout,  je vous encourage à vérifier si ce qui suit résonne avec votre expérience personnelle avant de l'intégrer ou même de le partager ☺ .

#### Conclusion

**Suite à la lecture de tout ça et aux diverses recherches que j'ai faites, voici mes théories** :

Comme pour une partie de Monopoly ou de ballon prisonnier, il est, d'après moi, nécessaire que tous les participants se mettent d'accord sur les règles du jeu avant de commencer. On n'aime généralement pas se retrouver dans la situation où quelqu'un se rappelle soudainement d'une règle qui remet tout en cause. Le cadre est important et le consentement de chacun à respecter ce cadre est extrêmement important.

Nous sommes tous différents, et bien que le groupe se doive de respecter l'individualisme et les différences de chacun, il est aussi vital que les individus respectent le bien-être du groupe. Donc  partir d'un socle commun et de valeurs communes clairement définis pour que chaque personne puisse avancer sans se demander si cela conviendrait au groupe à chaque fois qu'il est nécessaire de faire un choix.

Ensuite, il faut définir une vision commune, un objectif qui permettra de s'assurer que personne ne s'est trompé de bus en cours de chemin.


On aura, à mon avis, éliminé les risques qu'un personne profite d'une structure floue pour récupérer plus de pouvoir qu'il ne lui est dû, ou toute forme de conflit lié à des non dits, ou des mécanismes laissés à l'interprétation des participants. Si une valeur fondatrice votée par le groupe est le partage équitable des ressources, il y a moins de chances qu'une personne mange tout le gâteau pour ensuite dire qu'elle n'était pas au courant.

Restent ensuite les cas particuliers, les malentendus, les problèmes de communication ou même les personnes malhonnêtes.

**La première étape est la détection et la prise de conscienc**e. A mon avis (et encore une fois, ce n'est que mon avis :-) ), il est important qu'au moindre doute, à la moindre tension, la situation soit signalée au groupe. Les émotions telles que la colère et la frustration, si elles ne sont pas exprimées, ont une fâcheuse tendance à s'envenimer, s'enraciner et s'exprimer indirectement de mille manières néfastes au groupe. Il est, à court terme, plus facile en tant que groupe d'ignorer un désaccord entre 2 personnes, mais ce qui n'était qu'une courte clarification à effectuer peut vite se transformer en abcès purulent.

Donc la question est : quel mécanisme adopter pour s'assurer que les conflits et tensions soient détectés et mis en lumière le plus rapidement possible ?

**Est-ce qu'il s'agit d'en faire un devoir de vigilance pour chaque membre ? Un jeu ? Une obligation ?**


**La seconde étape est le traitement de ces tensions. Quels sont les mécanismes les plus efficaces pour la résolution de conflits ? S'ils sont pris assez tôt, ils devraient être moins compliqués à traiter, mais si ça dure depuis 6 mois et que le groupe est paralysé ?**


Une méthode radicale serait l'amputation. On identifie le périmètre problématique, le ou les individus et on les exclue. Mais quel message cela donne-t-il au reste du groupe ? Attention, s'il y a désaccord, vous êtes exclus ? D'après certaines études, le sentiment de sécurité émotionnelle est vecteur de performance et de créativité au sein d'un groupe, et j'ai pas trop l'impression que se dire qu'on peut se faire virer à tout moment donne un sentiment de sécurité :-) .

On peut faire appel à un élément extérieur. Une personne neutre, objective qui n'a pas d'intérêt à ce que la situation évolue dans une direction ou une autre, ou qui ne va pas se laisser influencer. J'ai personnellement déjà essayé de faire le médiateur dans un groupe auquel j'appartenais, et j'ai échoué. Deux fois. Dans deux groupes différents. Il est tout à fait possible que je sois juste médiocre en tant que médiateur mais je pense que si une personne est amenée dans le groupe avec cette unique mission et qu'elle en ressortira ensuite, son approche va être radicalement différente.

Je pense qu'il faut remonter soit aux besoins des personnes concernées comme le préconise la communication non violente, soit à leur intérêt comme le conseille la négociation raisonnée pour d'abord ramener l'individu dans une posture d'écoute. Une fois qu'il aura été entendu et qu'il aura pu exprimer ses besoins, il sera mieux à même de s'ouvrir aux besoins du groupe.

Beaucoup de techniques utilisées pour faciliter l'émergence de l'intelligence collectives sont aussi très utiles. Si la communication et la bienveillance font déjà partie intégrante de la culture de votre groupe, il sera d'autant plus facile de les appliquer.

Cela nécessite, comme l'une des réponses le mentionne, de faire le point régulièrement en tant que groupe pour s'assurer que les volontés sont toujours bien alignées. Les cercles restauratifs semblent également être un outil efficace, qui permet finalement de diffuser l'attention (la tension ?) auparavant concentrée sur les protagonistes du conflit et de mettre tous les membres du groupe à niveau égal.

Ci-dessous, j'ai listé différentes ressources, principalement sous forme de vidéo, qui pourront peut-être vous aider à choisir le mécanisme le plus efficace pour votre groupe.

Merci également pour ceux qui ont été partagés dans les réponses au questionnaire.

#### Sources et liens utiles

[Cette video](https://youtu.be/Lbt8mkHj8CI) (en anglais malheureusement) explique les différents types de mécanisme qui peuvent être utilisés pour résoudre les conflits, avec les différents avantages et inconvénients en fonction des protagonistes, de la situation, etc....

[La négociation raisonnée](http://www.circ-ien-andolsheim.ac-strasbourg.fr/IMG/pdf/poly_negocier_fisher-ury_cle8b4d6f.pdf) focalise entre autres, l'attention sur les intérêts des protagonistes pour les clarifier et les amener à trouver ensemble une destination satisfaisante pour tous.

[La communication non violente](http://www.cnvformations.fr/index.php?m=10&ms=118) se concentre sur l'expression des besoins d'une manière respectueuse en faisant attention à ne pas les imposer à l'autre.

Les modèles de gouvernance tels que la [sociocratie](https://colhor.wordpress.com/videos/) et la [holacratie](https://fr.wikipedia.org/wiki/Holacratie) intègrent dans leurs mécanismes de réunion des moyens de traiter les tensions au fur et à mesure qu'elles surviennent.

[Les cercles restauratifs](http://cerclesrestauratifs.org/wiki/Cercles_Restauratifs) sont une manière de mettre tous les protagonistes sur un pied d'égalité.

Voilà, c'est tout ce que j'ai... :-)
